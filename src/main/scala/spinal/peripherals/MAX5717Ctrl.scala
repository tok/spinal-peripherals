package spinal.peripherals

import spinal.core._
import spinal.lib._
import spinal.lib.bus.misc.BusSlaveFactory
import spinal.lib.fsm._

/**
  * Interface to MAX5717 DAC.
  */
case class MAX5717SPI() extends Bundle with IMasterSlave {
  val CS = Bool
  val LDAC = Bool
  // Latch control
  val SCLK = Bool
  val D_in = Bool

  override def asMaster(): Unit = {
    List(CS, LDAC, SCLK, D_in).foreach(out(_))
  }
}

/**
  * Controller for the MAX5717 DAC. This version uses a separate clock domain that matches the SPI clock.
  *
  * @param spiClockFreq Frequency of the SPI clock.
  */
class MAX5717CtrlSpiClock(spiClockFreq: HertzNumber = 1 MHz) extends Component {

  val dacResolution = 16
  require(dacResolution % 2 == 0)

  val io = new Bundle {
    val cmd = slave Stream (UInt(dacResolution bits))
    val spi = master(MAX5717SPI()).addTag(crossClockDomain)
  }


  def driveFrom(busCtrl: BusSlaveFactory, baseAddress: BigInt) = new Area {
    val cmdFlow = busCtrl.createAndDriveFlow(io.cmd.payload, baseAddress, 0)
    val cmdStream = cmdFlow.toStream
    io.cmd << cmdStream
  }

  val divider = (ClockDomain.current.frequency.getValue / spiClockFreq).toInt
  val clkCounter = Reg(UInt(log2Up(divider) bits)) init (0)
  when(clkCounter === divider - 1) {
    clkCounter := 0
  } otherwise {
    clkCounter := clkCounter + 1
  }

  val spiCLK = clkCounter >= divider / 2

  val spiClockDomain = ClockDomain(spiCLK, clockDomain.reset, frequency = FixedFrequency(spiClockFreq),
    config = ClockDomain.current.config.copy(clockEdge = FALLING))


  // Clock domain crossing.
  val cmd = StreamCCByToggle(
    input = io.cmd,
    inputClock = clockDomain,
    outputClock = spiClockDomain
  )

  val area = new ClockingArea(spiClockDomain) {


    val dataBuf = Reg(cmd.payloadType) init (0)

    io.spi.SCLK := False
    io.spi.CS := True
    io.spi.LDAC := False
    io.spi.D_in := False

    cmd.ready := False

    val fsm = new StateMachine {

      // Wait for an input.
      val sReady: State = new State with EntryPoint {
        whenIsActive {
          cmd.ready := True
          when(cmd.valid) {
            dataBuf := cmd.payload
            goto(sSerialize)
          }
        }
      }


      // Serialize the input to the SPI device.
      val sSerialize = new StateDelay(dacResolution) {
        whenIsActive {
          io.spi.SCLK := ClockDomain.current.readClockWire
          io.spi.CS := False
          io.spi.D_in := dataBuf.msb
          dataBuf := (dataBuf << 1).resized
        }
        whenCompleted {
          goto(sLdacHigh)
        }

      }

      // Let LDAC be low for the minimal value.
      val ldacPulseWidth = 20 ns // TODO: 20 ns
      val ldacPulseWidthCycles = (ClockDomain.current.frequency.getValue * ldacPulseWidth).toInt
      val sLdacHigh = new StateDelay(ldacPulseWidthCycles) {
        whenIsActive {
          io.spi.LDAC := True
        }
        whenCompleted {
          goto(sReady)
        }
      }
    }
  }
}

/**
  * Controller for the MAX5717 DAC. This version does NOT use a separate clock domain for the SPI clock.
  *
  * @param spiClockFreq Frequency of the SPI clock.
  */
class MAX5717Ctrl(spiClockFreq: HertzNumber = 1 MHz) extends Component {

  val dacResolution = 16
  require(dacResolution % 2 == 0)

  val io = new Bundle {
    val cmd = slave Stream (UInt(dacResolution bits))
    val spi = master(MAX5717SPI())
  }

  def driveFrom(busCtrl: BusSlaveFactory, baseAddress: BigInt) = new Area {
    val cmdFlow = busCtrl.createAndDriveFlow(io.cmd.payload, baseAddress, 0)
    val cmdStream = cmdFlow.toStream
    io.cmd << cmdStream
  }

  val divider = (ClockDomain.current.frequency.getValue / spiClockFreq).toInt
  val clkCounter = Reg(UInt(log2Up(divider + 1) bits)) init (0)
  val resetClock = False
  when(clkCounter === divider - 1 || resetClock) {
    clkCounter := 0
  } otherwise {
    clkCounter := clkCounter + 1
  }

  val spiCLK = clkCounter >= divider / 2
  val risingEdgeTick = clkCounter === divider / 2 - 1
  val fallingEdgeTick = clkCounter === divider - 1

  io.spi.SCLK := False

  val dataBuf = Reg(io.cmd.payloadType) init (0)

  io.spi.CS := True
  io.spi.LDAC := True
  io.spi.D_in := False

  io.cmd.ready := False

  val fsm = new StateMachine {

    // Wait for an input.
    val sReady: State = new State with EntryPoint {
      whenIsActive {
        io.cmd.ready := True
        when(io.cmd.valid) {
          dataBuf := io.cmd.payload
          //resetClock := True
          goto(sSync)
        }
      }
    }

    val sSync: State = new State {
      whenIsActive {
        //io.spi.SCLK := spiCLK
        when(fallingEdgeTick) {
          goto(sSerialize)
        }
      }
    }

    val counter = Reg(UInt(log2Up(dacResolution) bits)) init (0)
    // Serialize the input to the SPI device.
    val sSerialize = new State {
      onEntry {
        counter := dacResolution - 1
      }
      whenIsActive {

        io.spi.SCLK := spiCLK
        io.spi.CS := False

        io.spi.D_in := dataBuf.msb

        when(fallingEdgeTick) {
          when(counter > 0) {
            dataBuf := (dataBuf << 1).resized
            counter := counter - 1
          } otherwise {
            goto(sWait)
          }
        }
      }
    }

    // Wait until LDAC can be pulled low.
    val sWait = new State {
      whenIsActive {
        when(risingEdgeTick) {
          goto(sLdacHigh)
        }
      }
    }

    val ldacPulseWidth = 20 ns // TODO: 20 ns
    val ldacPulseWidthCycles = (ClockDomain.current.frequency.getValue * ldacPulseWidth).toInt + 1
    val sLdacHigh = new StateDelay(ldacPulseWidthCycles) {
      whenIsActive {
        io.spi.LDAC := False
      }
      whenCompleted {
        goto(sReady)
      }
    }
  }

}

object MAX5717Ctrl {
  def main(args: Array[String]) {
    //    SpinalConfig(
    //      targetDirectory = "rtl",
    //      defaultClockDomainFrequency = FixedFrequency(100 MHz)
    //    ).dumpWave().generateVerilog(new MAX5717Ctrl(50 MHz))
    //

    import spinal.core._
    import spinal.core.sim._
    import spinal.sim._

    val config = SpinalConfig(
      targetDirectory = "rtl",
      defaultClockDomainFrequency = FixedFrequency(100 MHz)
    )

    //val spinalVerilog = SpinalVerilog(config)(new MAX5717CtrlSpiClock(1 MHz))
    val spinalVerilog = SpinalVerilog(config)(new MAX5717Ctrl(10 MHz))

    SimConfig(rtl = spinalVerilog).withWave.doManagedSim { dut =>
      fork {
        dut.clockDomain.assertReset()
        dut.clockDomain.fallingEdge()
        sleep(10)
        while (true) {
          dut.clockDomain.clockToggle()
          sleep(5)
        }
      }

      repeatSim(1) {

        dut.io.cmd.valid #= true
        dut.io.cmd.payload #= 0x400f

        var i = 0
        while (i < 10000) {
          dut.clockDomain.waitActiveEdge()
          i += 1
        }
      }
    }
  }
}