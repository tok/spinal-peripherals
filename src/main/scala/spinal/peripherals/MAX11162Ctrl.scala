package spinal.peripherals

import spinal.core._
import spinal.lib._
import spinal.lib.bus.misc.BusSlaveFactory
import spinal.lib.fsm._

/**
  * Interface to MAX11162 ADC.
  */
case class MAX11162SPI() extends Bundle with IMasterSlave {
  val SDO = Bool
  val SDI = Bool
  val SCLK = Bool
  val CNVST = Bool

  override def asMaster(): Unit = {
    in(SDO)
    out(SCLK)
    out(CNVST) // Conversion start.
    out(SDI)
  }
}

/**
  * Controller for the MAX11162 ADC. This version uses a separate clock domain that matches the SPI clock.
  * @param spiClockFreq Frequency of the SPI clock.
  */
class MAX11162SpiClock(spiClockFreq: HertzNumber = 1 MHz) extends Component {

  val adcResolution = 16
  require(adcResolution % 2 == 0)

  val maxConversionTime = 1.6 us
  val cnvstPulseWidth = 20 ns // = t_cnvpw

  val io = new Bundle {
    val cmd = slave Stream (Bits(0 bits))
    val rsp = master Flow (UInt(adcResolution bits))
    val spi = master(MAX11162SPI()).addTag(crossClockDomain)
    val estimatedSampleRate = out(Reg(UInt(32 bits)) init (0))
  }

  // Sample rate estimation by counting samples within a second.
  val sampleCounter = Reg(io.estimatedSampleRate) init (0)
  val secondCounter = new Counter(0, (ClockDomain.current.frequency.getValue * (1 sec)).toBigInt)
  secondCounter.increment()
  when(secondCounter.willOverflow) {
    io.estimatedSampleRate := sampleCounter
    sampleCounter.clearAll()
  } otherwise {
    when(io.rsp.valid) {
      sampleCounter := sampleCounter + 1
    }
  }

  val divider = (ClockDomain.current.frequency.getValue / spiClockFreq).toInt
  val clkCounter = Reg(UInt(log2Up(divider) bits)) init (0)
  when(clkCounter === divider - 1) {
    clkCounter := 0
  } otherwise {
    clkCounter := clkCounter + 1
  }

  val spiCLK = clkCounter >= divider / 2

  val spiClockDomain = ClockDomain(spiCLK, clockDomain.reset, frequency = FixedFrequency(spiClockFreq),
    config = ClockDomain.current.config.copy(clockEdge = FALLING))

  // Clock domain crossing.
  val cmd = Stream(io.cmd.payload)
  cmd.ready := False
  cmd << StreamCCByToggle(
    input = io.cmd,
    inputClock = clockDomain,
    outputClock = spiClockDomain
  )

  val rsp = Flow(io.rsp.payload)
  rsp.valid := False
  rsp.payload := 0

  io.rsp << FlowCCByToggle(
    input = rsp,
    inputClock = spiClockDomain,
    outputClock = clockDomain
  )


  val area = new ClockingArea(spiClockDomain) {

    val dataBuf = Reg(UInt(adcResolution bits)) init (0)

    io.spi.CNVST := False
    io.spi.SDI := True
    io.spi.SCLK := False

    // Main state machine.
    val fsm = new StateMachine {

      // Wait for slave to be ready.
      val sIdle: State = new State with EntryPoint {
        whenIsActive {
          cmd.ready := True
          when(cmd.valid) {
            io.spi.CNVST := True
            goto(sWaitForConversion)
          }
        }
      }

      val conversionWaitCycles = (ClockDomain.current.frequency.getValue * maxConversionTime).toInt + 1

      val sWaitForConversion: State = new StateDelay(conversionWaitCycles) {
        whenIsActive {
          io.spi.CNVST := True
        }
        whenCompleted {
          goto(sDelay_t_en)
        }
      }

      val t_en = 20 ns
      val t_enCycles = (ClockDomain.current.frequency.getValue * t_en).toInt + 1
      val sDelay_t_en = new StateDelay(t_enCycles) {
        whenCompleted {
          goto(sRetrieveSample)
        }
      }

      val sRetrieveSample: State = new StateDelay(adcResolution) {
        whenIsActive {
          // Enable SPI clock.
          io.spi.SCLK := ClockDomain.current.readClockWire
          // Shift data bits in.
          dataBuf := (dataBuf ## io.spi.SDO).asUInt.resized
        }
        whenCompleted {
          goto(sDone)
        }
      }

      val sDone: State = new State {
        whenIsActive {
          rsp.payload := dataBuf
          rsp.valid := True
          goto(sDelay)
        }
      }

      val t_dis = 20 ns
      val delayCycles = (ClockDomain.current.frequency.getValue * t_dis).toInt + 1
      val sDelay = new StateDelay(delayCycles) {
        whenCompleted {
          goto(sIdle)
        }
      }

    }

  }
}

/**
  * Controller for the MAX11162 ADC. This version does NOT use a separate clock domain for the SPI clock.
  * @param spiClockFreq Frequency of the SPI clock.
  */
class MAX11162Ctrl(spiClockFreq: HertzNumber = 1 MHz) extends Component {

  val adcResolution = 16
  require(adcResolution % 2 == 0)

  val maxConversionTime = 20 us //1.6 us
  val cnvstPulseWidth = 20 ns // = t_cnvpw

  val io = new Bundle {
    val cmd = slave Stream (Bits(0 bits))
    val rsp = master Flow (UInt(adcResolution bits))
    val spi = master(MAX11162SPI())
    val estimatedSampleRate = out(Reg(UInt(32 bits)) init (0))
  }

  io.cmd.ready := False
  io.rsp.valid := False
  io.rsp.payload := 0

  def driveFrom(busCtrl: BusSlaveFactory, baseAddress: BigInt) = new Area {
    val triggerReg = busCtrl.createReadAndWrite(io.cmd.valid, baseAddress + 0, 0)
    val payloadReg = busCtrl.createReadOnly(io.rsp.payload, baseAddress + 4, 0)
    busCtrl.read(io.estimatedSampleRate, baseAddress + 8, 0)
    io.cmd.valid := triggerReg
    when(io.rsp.valid) {
      payloadReg := io.rsp.payload
      triggerReg.clear()
    }
  }

  // Sample rate estimation by counting samples within a second.
  val sampleCounter = Reg(io.estimatedSampleRate) init (0)
  val secondCounter = new Counter(0, (ClockDomain.current.frequency.getValue * (1 sec)).toBigInt)
  secondCounter.increment()
  when(secondCounter.willOverflow) {
    io.estimatedSampleRate := sampleCounter
    sampleCounter.clearAll()
  } otherwise {
    when(io.rsp.valid) {
      sampleCounter := sampleCounter + 1
    }
  }

  val divider = (ClockDomain.current.frequency.getValue / spiClockFreq).toInt
  val clkCounter = Reg(UInt(log2Up(divider + 1) bits)) init (0)
  val resetClock = False
  when(clkCounter === divider - 1 || resetClock) {
    clkCounter := 0
  } otherwise {
    clkCounter := clkCounter + 1
  }

  val spiCLK = clkCounter >= divider / 2
  val risingEdgeTick = clkCounter === divider / 2 - 1
  val fallingEdgeTick = clkCounter === divider - 1
  //val clockLowTick = clkCounter === divider / 4 * 1 - 1
  //val clockHighTick = clkCounter === divider * 3 / 4 - 1

  val dataBuf = Reg(UInt(adcResolution bits)) init (0)

  io.spi.CNVST := False
  io.spi.SDI := True
  io.spi.SCLK := False

  // Main state machine.
  val fsm = new StateMachine {

    val busyIndicator = false

    // Wait for slave to be ready.
    val sIdle: State = new State with EntryPoint {
      whenIsActive {
        io.cmd.ready := True
        when(io.cmd.valid) {
          goto(sCnvstPulse)
        }
      }
    }

    // Wait for minimal CNVST pulse width.
    val cnvstPulseWidthCycles = (ClockDomain.current.frequency.getValue * cnvstPulseWidth).toInt + 1
    val sCnvstPulse: State = new StateDelay(cnvstPulseWidthCycles) {
      whenIsActive {
        io.spi.CNVST := True
      }
      whenCompleted {
        goto(sWaitForConversion)
      }
    }

    val conversionWaitCycles = (ClockDomain.current.frequency.getValue * maxConversionTime).toInt + 1
    // Wait for conversion to complete.
    assert(conversionWaitCycles > cnvstPulseWidthCycles)
    val sWaitForConversion: State = new StateDelay(conversionWaitCycles - cnvstPulseWidthCycles) {
      if (busyIndicator) {
        whenIsActive {
          when(!io.spi.SDO) { // Wait for busy bit to be low.
            resetClock := True
            goto(sSync)
          }
        }
        whenCompleted {
          resetClock := True
          goto(sSync)
        }
      } else {
        whenIsActive {
          io.spi.CNVST := True
        }
        whenCompleted {
          resetClock := True
          goto(sRetrieveSample)
        }
      }
    }

    //    val sWaitForConversion: State = new State {
    //      whenIsActive {
    //        when(!io.spi.SDO) { // Wait for busy bit to be low.
    //          resetClock := True
    //          goto(sSync)
    //        }
    //      }
    //    }

    val sSync: State = new State {
      whenIsActive {
        io.spi.SCLK := spiCLK
        when(fallingEdgeTick) {
          goto(sRetrieveSample)
        }
      }
    }

    val counter = Reg(UInt(log2Up(adcResolution) bits)) init (0)
    val sRetrieveSample: State = new State {
      onEntry(counter := adcResolution - 1)
      whenIsActive {
        // Enable SPI clock.
        io.spi.SCLK := spiCLK

        when(fallingEdgeTick) {
          // Shift data bits in.
          dataBuf := (dataBuf ## io.spi.SDO).asUInt.resized
          counter := counter - 1
        }

        when(counter === 0 && fallingEdgeTick) {
          goto(sDone)
        }
      }
    }


    val sDone: State = new State {
      whenIsActive {
        io.rsp.payload := dataBuf
        io.rsp.valid := True
        goto(sDelay)
      }
    }

    val t_dis = 20 ns
    val delayCycles = (ClockDomain.current.frequency.getValue * t_dis).toInt + 1
    val sDelay = new StateDelay(delayCycles) {
      whenCompleted {
        goto(sIdle)
      }
    }
  }
}

object MAX11162Ctrl {
  def main(args: Array[String]) {

    import spinal.core._
    import spinal.core.sim._
    import spinal.sim._

    val config = SpinalConfig(
      targetDirectory = "rtl",
      defaultClockDomainFrequency = FixedFrequency(100 MHz)
    )

    //val spinalVerilog = SpinalVerilog(config)(new MAX11162SpiClock(10 MHz))
    val spinalVerilog = SpinalVerilog(config)(new MAX11162Ctrl(1 MHz))


    SimConfig(rtl = spinalVerilog).withWave.doManagedSim { dut =>
      fork {
        dut.clockDomain.assertReset()
        dut.clockDomain.fallingEdge()
        sleep(10)
        while (true) {
          dut.clockDomain.clockToggle()
          sleep(5)
        }
      }

      repeatSim(1) {

        dut.io.cmd.valid #= true

        var i = 0
        while (i < 1000) {
          dut.clockDomain.waitActiveEdge()
          i += 1
        }
      }
    }
  }
}