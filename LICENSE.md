The code of this project is licensed under the weak reciprocal CERN Open Hardware License v2.0 (OHL-L).
(Note: this allows to use the library in any other project. Modifications to the library itself must be released under the same license again.)
