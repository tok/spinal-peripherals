# Spinal Peripherals
This is a collection of controller logic for peripheral devices such as SPI ADC/DACs written in SpinalHDL.

## Supported components
* MAX11162 16-bit SAR ADC
* MAX5717 16-bit DAC
